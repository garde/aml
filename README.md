AML challenges notebooks. 

Using Binder for collective edit. Click on the button below to launch the environment!
Do not forget to download and push the updated notebook once you are done!

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.eurecom.fr%2Fgarde%2Faml/master)
